sysinfo (0.7-8) unstable; urgency=low

  * Team upload
  * No change rebuild for CLR 4.0 transition
  * [b636e7f] Add patch to fix automake error with pkglibdir
  * [fb5b84b] Drop deprecated cli.make include

 -- Chow Loong Jin <hyperair@debian.org>  Thu, 19 Jan 2012 03:58:43 +0800

sysinfo (0.7-7) unstable; urgency=low

  * Team upload
  * [e37ae5b] Imported Upstream version 0.7
  * [c876b70] Update Vcs-* fields to point to git
  * [1718bf7] Move to 3.0 (quilt) format
  * [ed26925] Add patch to fix NRE when /proc/cpuinfo has no bogomips.
    Thanks to Douglas Mencken (Closes: #654210)
  * [6135b9b] Add patch to remove cpuinfo ordering assumption
  * [35d4469] Refresh 10-better_xorg_parsing.diff
    dpkg-source pukes on it otherwise.

 -- Chow Loong Jin <hyperair@debian.org>  Wed, 11 Jan 2012 02:14:47 +0800

sysinfo (0.7-6) unstable; urgency=low

  * debian/rules, debian/control: Include cli.make so correct dependencies are
    generated.
  * Run wrap-and-sort

 -- Iain Lane <laney@debian.org>  Thu, 16 Jun 2011 11:59:04 +0100

sysinfo (0.7-5) unstable; urgency=low

  * Upload to unstable
  * debian/patches/11-typo.diff: Apply patch from Ubuntu to fix typo
    Multmedia → Multimedia in the interface

 -- Iain Lane <laney@debian.org>  Fri, 03 Jun 2011 14:18:36 +0100

sysinfo (0.7-4) experimental; urgency=low

  * debian/rules, debian/patches/series{,-ubuntu}: Drop Ubuntu specific
    patches: patch will have no effect if affected file is not on the
    system and thus is harmless to have on Debian.
  * debian/patches/10_better-xorg-parsing.patch: Add new patch to improve
    Xorg log parsing to work correctly on Debian (and with timestamped
    logs) and not to crash the program on a malformed/empty input file.
    (Closes: #626196)
  * debian/*: Update packaging to DH7 & dh-autoreconf, include README.source
  * debian/control:
    + Update Homepage field to sf.net page
    + Standards-Version → 3.9.3, no changes required
  * debian/watch: Correctly sort beta versions lower than releases

 -- Iain Lane <laney@ubuntu.com>  Tue, 02 Feb 2010 00:35:30 +0000

sysinfo (0.7-3) unstable; urgency=low

  [ David Paleino ]
  * debian/control:
    + updated my e-mail address

  [ Jo Shields ]
  * debian/patches/00-fix_build_system.patch,
    debian/control:
    + Use mono-csc from mono-devel 2.4.3 (Closes: #562280)
  * debian/control:
    + No-change bump to Standards 3.8.3
    + Use GNOME# and GTK# -dev packages
    + Add a Suggests on nvidia-settings (Closes: #557574)

 -- Jo Shields <directhex@apebox.org>  Thu, 07 Jan 2010 17:38:05 +0000

sysinfo (0.7-2) unstable; urgency=low

  [ Iain Lane ]
  * Import bugfix patches from Ubuntu.
  * debian/patches/02-fix_sysinfo.desktop.patch: More cleanups, thanks to
    Pedro Fragoso
  * debian/patches/07-nvidia_crash.diff: Fix crasher when clicking Nvidia
    icon, thanks to Koen Beek
  * debian/patches/06-check_nvidia_settings.patch: Don't show Nvidia options
    if nvidia-settings is not installed, thanks to Koen Beek
  * debian/patches/08-correct_url.diff: Show correct upstream URL, old one is
    dead and has been squatted, thanks to Koen Beek
  * debian/patches/09-ubuntu_lsb_release.diff: Ubuntu specific - use
    lsb_release file if it is found to parse distro information, thanks to
    Koen Beek
  * debian/patches/04-fix_usb_pci_limit.patch: Allow an arbitrary number of
    PCI/USB devices to be displayed, thanks to Koen Beek
  * debian/rules, debian/patches/series-ubuntu: Only apply Ubuntu patches
    when building on Ubuntu
  * debian/menu: Alter section to comply with menu sub-policy

  [ Mirco Bauer ]
  * Upload to unstable.
  * debian/control:
    + Added libmono-cairo1.0-cil to build-deps as that one contains the
      pkg-config file.

 -- Iain Lane <laney@ubuntu.com>  Fri, 06 Mar 2009 00:53:26 +0100

sysinfo (0.7-1) experimental; urgency=low

  [ David Paleino ]
  * Initial release (Closes: #507187)

  [ Mirco Bauer ]
  * debian/control:
    + Enhanced package description.
    + Added Vcs-* fields.

 -- David Paleino <d.paleino@gmail.com>  Thu, 20 Nov 2008 19:29:07 +0100
